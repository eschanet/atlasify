#!/usr/bin/env python3

import argparse
import re
import sys

def check_file(file_path):
    """
    Check that a python module does not contain duplicated test names. The
    method returns a list of duplicated method names.
    """
    method_list = set()
    class_list = set()
    duplicates = []
    with open(file_path) as file:
        for line in file:
            match = re.match("^class ([a-zA-Z0-9_]*)[:(]", line)
            if match:
                # New class, clear list of methods
                method_list = set()
                class_name = match.group(1)
                if class_name in class_list:
                    duplicates.append(class_name)
                else:
                    class_list.add(class_name)

            match = re.match("^\s*def ([a-zA-Z0-9_]*)\(", line)
            if match:
                # Found method
                method_name = match.group(1)
                if method_name in method_list:
                    duplicates.append(method_name)
                else:
                    method_list.add(method_name)
    return duplicates



def check(files):
    """
    Calls check_file() for each file in the argument. The method returns
    False, if duplications are found. The method prints names all duplicated
    test cases.
    """
    success = True
    for file in files:
        duplicates = check_file(file)
        if duplicates:
            success = False
            print("Duplicates found in %s:" % file)
            for duplicate in duplicates:
                print("  %s" % duplicate)

    return success


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file", nargs="+", metavar="FILE", default=[],
                        help="Path to Python module to scan.")

    args = parser.parse_args()

    if not check(args.file):
        sys.exit(1)